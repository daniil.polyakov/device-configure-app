package com.example.myapplication.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.device.Command
import com.example.myapplication.device.Device
import com.example.myapplication.utils.ByteArrayUtils
import java.io.DataInputStream
import java.io.DataOutputStream

class DataAdapter(context: Context, private val devices: List<Device>) :
    RecyclerView.Adapter<DataAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val devicesToRunList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.device_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val device = devices[position]
        holder.deviceIdView.text = device.id
        holder.deviceSettingsBtn.setOnClickListener {
            val bundle = bundleOf("deviceId" to devices[position].id)

            Navigation.findNavController(holder.itemView).navigate(
                R.id.action_main_fragment_to_settings_fragment,
                bundle)
        }
        holder.deviceIdView.setOnClickListener {
            run {
                Thread {
                    val socket = device.socket
                    val oStr = DataOutputStream(socket.getOutputStream())
                    val iStr = DataInputStream(socket.getInputStream())
                    val hexString = device.id + Command.SYSLOG
                    oStr.write(ByteArrayUtils.hexStringToByteArray(hexString))
                    val byteArr = ByteArray(15)
                    iStr.readFully(byteArr, 0, byteArr.size)
                    println("TCP received: " + ByteArrayUtils.byteArrayToHexString(byteArr))
                }.start()
            }
        }
    }

    override fun getItemCount() = devices.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val deviceIdView : TextView = view.findViewById(R.id.text_device_id)
        val deviceSettingsBtn : Button = view.findViewById(R.id.device_settings_button)
    }

}