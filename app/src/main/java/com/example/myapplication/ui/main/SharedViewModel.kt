package com.example.myapplication.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.device.Device
import java.net.Socket

class SharedViewModel : ViewModel() {

    private var devicesConnectedLiveData = MutableLiveData<Map<String, Device>>()
    private var devicesConnected = HashMap<String, Device>()
    var lastDeviceId = MutableLiveData<String>()
    var deviceIdsToRun = mutableListOf<String>()
    var deviceIdsToRunLiveData = MutableLiveData<List<String>>()

    fun addConnectedDevice(id: String, socket: Socket) {
        devicesConnected[id] = Device(id, socket)
        devicesConnectedLiveData.value = devicesConnected
    }

    fun clearConnectedDevicesInfo() {
        devicesConnected = hashMapOf()
        devicesConnectedLiveData.value = devicesConnected
    }

    fun getAllDevicesMutableLiveData() = devicesConnectedLiveData

    fun getDeviceById(id: String) = devicesConnected[id]

    fun addDeviceToRun(id: String) {
        deviceIdsToRun.add(id)
        deviceIdsToRunLiveData.value = deviceIdsToRun
    }

    fun removeDeviceToRun(id: String) {
        deviceIdsToRun.remove(id)
        deviceIdsToRunLiveData.value = deviceIdsToRun
    }

}