package com.example.myapplication.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.device.Command
import com.example.myapplication.device.Device
import com.example.myapplication.utils.ByteArrayUtils
import com.example.myapplication.utils.WifiApInfoParser
import kotlinx.android.synthetic.main.main_fragment.*
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.Socket
import java.net.SocketTimeoutException

class MainFragment : Fragment() {

    private lateinit var viewModel: SharedViewModel
    private var deviceListener : DeviceListener? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)

        viewModel.lastDeviceId.value = "none"

        button_search.setOnClickListener {
            deviceListener = DeviceListener()
            deviceListener!!.start()
        }
        button_stop.setOnClickListener {
            deviceListener?.stopListening()
        }

        recyclerView = list_devices

        viewModel.getAllDevicesMutableLiveData().observe(viewLifecycleOwner, {
                deviceMap -> run {
            viewAdapter = DataAdapter(requireContext(), deviceMap.values.toList())
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = viewAdapter
        }
        })
    }



    private inner class DeviceListener : Thread() {

        private lateinit var datagramSocket : DatagramSocket
        private lateinit var tcpSocket : Socket
        @Volatile private var isListening = true
        private val BUFFER_SIZE = 15
        private var lastId = ""
        private lateinit var lastTcpSocket : Socket

        override fun run() {
            var id : String
            val buffer = ByteArray(BUFFER_SIZE)
            val devicePacket = DatagramPacket(buffer, buffer.size)

            try {
                datagramSocket = DatagramSocket(9998)
                datagramSocket.soTimeout = 3000

                activity!!.runOnUiThread {
                    Toast.makeText(context, "listening started!", Toast.LENGTH_LONG).show()
                }

                while (isListening) {
                    try {
                        datagramSocket.receive(devicePacket)
                        id = ByteArrayUtils.byteArrayToHexString(buffer.copyOfRange(0, 6))
                        lastId = id

                        val address = devicePacket.address
                        val mac = WifiApInfoParser.getMacAddress(address.hostAddress)

                        println("MAC: $mac")

                        tcpSocket = Socket(address, 9995)
                        lastTcpSocket = tcpSocket

                        activity!!.runOnUiThread {
                            if (deviceListener != null) {
                                val device = deviceListener!!.getLast()
                                viewModel.addConnectedDevice(device.id, device.socket)
                                viewModel.lastDeviceId.value = device.id
                            }
                        }

                        println("connected: " + tcpSocket.isConnected)
                    } catch (e: SocketTimeoutException) {
                        println("no packet...")
                    }
                }
                activity!!.runOnUiThread {
                    Toast.makeText(context, "listening stopped!", Toast.LENGTH_LONG).show()
                }
            } catch (e : Throwable) {
                e.printStackTrace()
            }

            if (this::datagramSocket.isInitialized) datagramSocket.close()
        }

        fun stopListening() {
            isListening = false
        }

        fun getLast() : Device {
            return Device(lastId, lastTcpSocket)
        }
    }

}