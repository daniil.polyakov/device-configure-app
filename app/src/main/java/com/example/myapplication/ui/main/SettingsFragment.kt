package com.example.myapplication.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.device.Command
import com.example.myapplication.utils.ByteArrayUtils
import kotlinx.android.synthetic.main.fragment_settings.*
import java.io.DataOutputStream
import java.nio.ByteBuffer


class SettingsFragment : Fragment() {
    private lateinit var viewModel: SharedViewModel
    private var deviceId: String? = null

    private var currentOdr: String = "4000 Hz"
    private var currentG: String = "2g"
    private var currentMicAmp: String = "+0dB"
    private var currentMicWidth: String = "9 бит"

    private val odrMap = mapOf(
        "4000 Hz" to 0b0000, "2000 Hz" to 0b0001, "1000 Hz" to 0b0010, "500 Hz" to 0b0011,
        "250 Hz" to 0b0100, "125 Hz" to 0b0101, "62.5 Hz" to 0b0110, "31.25 Hz" to 0b0111,
        "15.625 Hz" to 0b1000, "7.813 Hz" to 0b1001, "3.906 Hz" to 0b1010
    )

    private val odrList = listOf(
        "4000 Hz", "2000 Hz", "1000 Hz", "500 Hz", "250 Hz", "125 Hz", "62.5 Hz", "31.25 Hz",
        "15.625 Hz", "7.813 Hz", "3.906 Hz"
    )

    private val gMap = mapOf("2g" to 0b0001, "4g" to 0b0010, "8g" to 0b0011)

    private val gList = listOf("2g", "4g", "8g")

    private val micAmpMap = mapOf(
        "+0dB" to 0b0000, "+2.5dB" to 0b0001, "+6dB" to 0b0010, "+11dB" to 0b0011
    )

    private val micAmpList = listOf("+0dB", "+2.5dB", "+6dB", "+11dB")

    private val micWidthMap = mapOf(
        "9 бит" to 0b0000, "10 бит" to 0b0001, "11 бит" to 0b0010, "12 бит" to 0b0011
    )

    private val micWidthList = listOf("9 бит", "10 бит", "11 бит", "12 бит")

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)

        device_id_view.text = deviceId

        val odrAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, odrList)
        val gAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, gList)
        val micAmpAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, micAmpList)
        val micWidthAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, micWidthList)

        odr_spinner.adapter = odrAdapter
        g_spinner.adapter = gAdapter
        mic_amp_spinner.adapter = micAmpAdapter
        mic_width_spinner.adapter = micWidthAdapter

        val odrSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val item = parent?.getItemAtPosition(position) as String
                currentOdr = item
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        odr_spinner.onItemSelectedListener = odrSelectedListener

        val gSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val item = parent?.getItemAtPosition(position) as String
                currentG = item
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        g_spinner.onItemSelectedListener = gSelectedListener

        val micAmpSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val item = parent?.getItemAtPosition(position) as String
                currentMicAmp = item
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        mic_amp_spinner.onItemSelectedListener = micAmpSelectedListener

        val micWidthSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val item = parent?.getItemAtPosition(position) as String
                currentMicWidth = item
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        mic_width_spinner.onItemSelectedListener = micWidthSelectedListener

        save_settings_button.setOnClickListener {
            run {
                Thread {
                    val odr = odrMap[currentOdr] ?: 0b0000
                    val g = gMap[currentG] ?: 0b0001
                    val micAmp = micAmpMap[currentMicAmp] ?: 0b0000
                    val micWidth = micWidthMap[currentMicWidth] ?: 0b0000
                    val firstByte = ((odr shl 4) xor g).toByte()
                    val secondByte = ((micAmp shl 4) xor micWidth).toByte()
                    val commandSettings = ByteArrayUtils.hexStringToByteArray(deviceId + Command.SETTING) + firstByte + secondByte
                    val device = deviceId?.let { it1 -> viewModel.getDeviceById(it1) }
                    val socket = device?.socket
                    val oStr = DataOutputStream(socket?.getOutputStream())
                    oStr.write(commandSettings)
                    requireActivity().runOnUiThread {
                        Toast.makeText(activity, "Sent to device!", Toast.LENGTH_LONG).show()
                    }
                }.start()
            }
        }

        check_device_start.setOnCheckedChangeListener {
                _, isChecked ->
            if (isChecked) {
                viewModel.addDeviceToRun(deviceId!!)
            } else {
                viewModel.removeDeviceToRun(deviceId!!)
            }
        }

        viewModel.deviceIdsToRunLiveData.observe(viewLifecycleOwner, {
                _ -> run {
            Toast.makeText(activity, "devices run list changed!", Toast.LENGTH_LONG).show()
        }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        deviceId = arguments?.getString("deviceId")
        return view
    }


}