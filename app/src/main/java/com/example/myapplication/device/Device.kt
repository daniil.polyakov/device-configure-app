package com.example.myapplication.device

import java.net.Socket

data class Device(val id: String, val socket: Socket)