package com.example.myapplication.device

enum class Command(private val hexString: String) {
    SYSLOG("00"),
    MODE("01"),
    SETTING("02"),
    START("03"),
    RESET("05"),
    REMOVE("06"),
    CHANGE_IP("07"),
    GET_ORIENTATION("08");

    override fun toString(): String {
        return hexString
    }
}