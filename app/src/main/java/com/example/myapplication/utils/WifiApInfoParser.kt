package com.example.myapplication.utils

import java.io.*

object WifiApInfoParser {

    private val ipRegex = "((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)".toRegex()
    private val macRegex = "([0-9a-f]{2}:){5}([0-9a-f]{2})".toRegex()

    fun getMacAddress(ipAddress : String) = getSomethingFromShell(ipAddress, macRegex)

    fun getIpAddress(macAddress : String) = getSomethingFromShell(macAddress, ipRegex)

    private fun getSomethingFromShell(given : String, findRegex: Regex) : String? {
        try {
            val process = Runtime.getRuntime().exec("ip neigh")

            val reader = BufferedReader(InputStreamReader(process.inputStream))
            var line = reader.readLine()
            while (line != null) {
                if (line.contains(given)) {
                    val match = findRegex.find(line)!!
                    return match.value
                }
                line = reader.readLine()
            }
        } catch (e : IOException) {
            e.printStackTrace()
        }
        return null
    }

}