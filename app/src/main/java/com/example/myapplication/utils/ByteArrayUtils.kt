package com.example.myapplication.utils

object ByteArrayUtils {

    fun byteArrayToHexString(array: ByteArray) : String {
        val sb = StringBuilder()
        for (b in array) {
            val st = String.format("%02X", b)
            sb.append(st)
        }
        return sb.toString()
    }

    fun hexStringToByteArray(hexString: String) : ByteArray {
        require(hexString.length % 2 != 1) { "Invalid hexadecimal String supplied." }
        val bytes = ByteArray(hexString.length / 2)
        var i = 0
        while (i < hexString.length) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2))
            i += 2
        }
        return bytes
    }

    private fun hexToByte(hexString: String) : Byte {
        val firstDigit = toDigit(hexString[0])
        val secondDigit = toDigit(hexString[1])
        return ((firstDigit shl 4) + secondDigit).toByte()
    }

    private fun toDigit(hexChar: Char) : Int {
        val digit = Character.digit(hexChar, 16)
        require(digit != -1) { "Invalid Hexadecimal Character: $hexChar" }
        return digit
    }

}