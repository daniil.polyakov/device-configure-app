package com.example.myapplication.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "modules")
class Module(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "ip_address") val ipAddress: String
)