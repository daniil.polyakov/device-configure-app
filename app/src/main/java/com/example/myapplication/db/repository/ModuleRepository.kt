package com.example.myapplication.db.repository

import com.example.myapplication.db.dao.ModuleDao
import com.example.myapplication.db.dao.SettingsDao
import com.example.myapplication.db.entity.Module
import com.example.myapplication.db.entity.Settings

class ModuleRepository(private val moduleDao: ModuleDao, private val settingsDao: SettingsDao) {

    suspend fun insertModule(module: Module) {
        moduleDao.insert(module)
    }

    suspend fun insertSettings(settings: Settings) {
        settingsDao.insert(settings)
    }

    fun getModule(moduleId: String) = moduleDao.getModule(moduleId)

    fun getModuleSettings(moduleId: String) = settingsDao.getModuleSettings(moduleId)

}