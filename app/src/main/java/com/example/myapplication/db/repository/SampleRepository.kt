package com.example.myapplication.db.repository

import com.example.myapplication.db.dao.SampleDao
import com.example.myapplication.db.entity.Sample

class SampleRepository(private val sampleDao: SampleDao) {

    suspend fun insertSample(sample: Sample) {
        sampleDao.insert(sample)
    }

    fun getModuleSamples(id: String) = sampleDao.getModuleSamples(id)
}