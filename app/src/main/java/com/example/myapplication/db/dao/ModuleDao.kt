package com.example.myapplication.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.myapplication.db.entity.Module

@Dao
interface ModuleDao {

    @Query("SELECT * FROM modules WHERE id = :id")
    fun getModule(id: String): LiveData<Module>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(module: Module)

}