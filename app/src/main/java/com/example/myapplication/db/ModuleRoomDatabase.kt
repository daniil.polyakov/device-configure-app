package com.example.myapplication.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.db.dao.ModuleDao
import com.example.myapplication.db.dao.SampleDao
import com.example.myapplication.db.dao.SettingsDao
import com.example.myapplication.db.entity.Module
import com.example.myapplication.db.entity.Sample
import com.example.myapplication.db.entity.Settings

@Database(
    entities = [Module::class, Sample::class, Settings::class],
    version = 1,
    exportSchema = false
)
abstract class ModuleRoomDatabase : RoomDatabase() {

    abstract fun moduleDao(): ModuleDao
    abstract fun sampleDao(): SampleDao
    abstract fun settingsDao(): SettingsDao

    companion object {
        @Volatile
        private var INSTANCE: ModuleRoomDatabase? = null

        fun getDatabase(context: Context): ModuleRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ModuleRoomDatabase::class.java,
                    "device_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}