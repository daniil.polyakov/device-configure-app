package com.example.myapplication.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.myapplication.db.entity.Settings

@Dao
interface SettingsDao {

    @Query("SELECT * FROM module_settings WHERE module_id = :id")
    fun getModuleSettings(id: String): LiveData<Settings>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(settings: Settings)
}