package com.example.myapplication.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.myapplication.db.entity.Sample

@Dao
interface SampleDao {

    @Insert
    suspend fun insert(sample: Sample): Long

    @Query("SELECT * FROM samples WHERE module_id = :id")
    fun getModuleSamples(id: String): LiveData<List<Sample>>

}