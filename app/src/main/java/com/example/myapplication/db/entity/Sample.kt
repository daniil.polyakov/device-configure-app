package com.example.myapplication.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "samples", foreignKeys = [
    ForeignKey(entity = Module::class, parentColumns = ["id"],
        childColumns = ["module_id"], onDelete = ForeignKey.CASCADE)
])
class Sample(
    @PrimaryKey(autoGenerate = true) val serial: Long? = null,
    @ColumnInfo(name = "module_id") val moduleId: String,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) val data: ByteArray
)