package com.example.myapplication.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "module_settings", foreignKeys = [
    ForeignKey(entity = Module::class, parentColumns = ["id"],
        childColumns = ["module_id"], onDelete = ForeignKey.CASCADE)
])
class Settings(
    @PrimaryKey @ColumnInfo(name = "module_id") val moduleId: String,
    val odr: String,
    val g: String,
    @ColumnInfo(name = "mic_amp") val micAmp: String,
    @ColumnInfo(name = "mic_width") val micWidth: String
)